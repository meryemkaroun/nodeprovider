const db = require("../models");
const romeCode = db.rome_code;

exports.findAll = async function () {
    return romeCode.find({});
};

exports.find = (req, res) => {
    romeCode.find()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || " Une erreur est survenue lors de la récupération des méties."
            });
        });
};