const db = require("../models");
const Offre = db.offres;

exports.create = (offre) => {
    // Create offer
    const offre1 = new Offre({
        id:  offre.id,
        address: offre.address,
        alternance: offre.alternance,
        city: offre.city,
        contact_mode: offre.contact_mode,
        headcount_text: offre.headcount_text,
        matched_rome_code: offre.matched_rome_code,
        matched_rome_label: offre.matched_rome_label,
        name: offre.name,
        siret: offre.siret,
        url: offre.url,
        stars: offre.stars,
        lat: offre.lat,
        lon: offre.lon
    });

    console.log('before saved offer');

    // Save Offer in the database
    offre1.save(offre1)
        .then(data => {
            console.log('saved offer');
        })
        .catch(err => {
            console.error(err);
        });
};

exports.findAll = (req, res) => {
    const code = req.query.rome_code.replace( /\s/g, '');
    console.log(code);
    Offre.find({'matched_rome_code': code}, function (err, data) {
        if(err) {
            console.log(err);
            res.status(500).send({
                message:
                    err.message || " une erreur est survenue lors de la récupération des offres."
            });
        }
        res.send(data);
    });
};


exports.indicators = async (req, res) => {
    const numberOfOffers = await Offre.count({});
    const companies = await Offre.find().distinct('name');
    const numberOfCompanies = companies.length;
    const cities = await Offre.find().distinct('city');
    const numberOfCities = cities.length;
    const jobs = await Offre.find().distinct('matched_rome_code');
    const numberOfJobs = jobs.length;

    const result = {
        numberOfOffers: numberOfOffers,
        numberOfCompanies: numberOfCompanies,
        numberOfCities: numberOfCities,
        numberOfJobs: numberOfJobs
    }
    res.send(result);
}

exports.findOffersByCity = (req, res) => {
    Offre.aggregate([
        {
            $group: {
                _id: "$city",
                count: {$sum: 1}
            }},
        {
            $sort: {'count': -1}
        },
        {
            $limit: 10
        }
    ], function (err, data) {
        if(err) {
            console.log(err);
            res.status(500).send({
                message:
                    err.message || " une erreur est survenue lors de la récupération des offres."
            });
        }
        res.send(data);
    });
}

exports.findCompaniesWithMostOffers = (req, res) => {
    Offre.aggregate([
        {
            $group: {
                _id: "$name",
                count: {$sum: 1}
            }},
        {
            $sort: {'count': -1}
        },
        {
            $limit: 10
        }
    ], function (err, data) {
        if(err) {
            console.log(err);
            res.status(500).send({
                message:
                    err.message || " une erreur est survenue lors de la récupération des offres."
            });
        }
        res.send(data);
    });
}

exports.findFavoriteOffersById = (req, res) => {
    const ids = req.query.offersIds.split(', ');
    Offre.find().where('_id').in(ids).exec((err, records) => {
        if(err) {
            res.status(500).send({
                message:
                    err.message || " une erreur est survenue lors de la récupération des ids."
            });
        }
        res.send(records);
    });
};







