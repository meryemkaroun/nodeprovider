module.exports = mongoose => {
    return mongoose.model(
        "offres",
        mongoose.Schema(
            {
                id: String,
                address: String,
                alternance: Boolean,
                city: String,
                contact_mode: String,
                headcount_text: String,
                matched_rome_code: String,
                matched_rome_label: String,
                name: String,
                siret: String,
                url: String,
                stars: Number,
                lat: Number,
                lon: Number
            },
            { timestamps: true },
        ),
        "offres"
    );
};