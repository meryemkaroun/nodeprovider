require('dotenv').config({path: "../.env"});

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = process.env.URL_MONGO;
db.offres = require("./offre.model.js")(mongoose);
db.rome_code = require("./rome_code.model.js")(mongoose);

module.exports = db;