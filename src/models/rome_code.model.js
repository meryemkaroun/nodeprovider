module.exports = mongoose => {
    return mongoose.model(
        "rome_code",
        mongoose.Schema(
            {
                id: String,
                code: String,
                metier: String,
            },
            { timestamps: true }
        ),
        "rome_code"
    );
};