require('dotenv').config();

const rp = require('request-promise');
const OffresController = require("./src/controllers/offres.controller");
const RomeCodeController = require("./src/controllers/romecode.controller");

const db = require("./src/models");

db.mongoose.connect(db.url, () =>
    console.log("Connected to the database!")
);

async function getAndSaveOffers(rome_code, token) {
    let options = {
        uri: process.env.URL_API + rome_code.code,
        headers: {
            'Authorization': 'Bearer ' + token.access_token
        },
        json: true
    };
    let offers = await rp(options);

    for (let offer of offers.companies) {
        OffresController.create(offer);
    }
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

rp.post('https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=/partenaire', {
    form: {
        grant_type: "client_credentials",
        client_id: "PAR_getyourjob_a85b4d82cdad60433575d6390ac2ff59c187f52d8c02e404833e52d66a165116",
        client_secret: "6948018791caeac1160f253c0de71be89399f9075b6c8d1b0d3b50121f167a77",
        scope: "application_PAR_getyourjob_a85b4d82cdad60433575d6390ac2ff59c187f52d8c02e404833e52d66a165116 api_labonneboitev1"
    },
    options: {
        json: true
    }
}).then(result => {
    let token = JSON.parse(result);
    RomeCodeController.findAll().then(async rome_codes => {
        for (let rome_code of rome_codes) {
            await getAndSaveOffers(rome_code, token);
            await sleep(1000);
        }
    });
});