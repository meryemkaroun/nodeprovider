require('dotenv').config()

const express = require('express');
const cors = require("cors");
const bodyParser = require('body-parser')
const TwitterApi = require('twitter-api-v2').TwitterApi;
const OffresController = require("./src/controllers/offres.controller");
const romecodeController = require("./src/controllers/romecode.controller");

// créer une instance d'Express
const app = express();

const twitterClient = new TwitterApi(process.env.bearer_token);

const corsOptions = {
    origin: "*"
};

const db = require("./src/models");

db.mongoose.connect(db.url, () =>
    console.log("Connected to the database!")
);

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

app.get("/metiers", romecodeController.find);
app.get("/offres", OffresController.findAll);
app.get("/indicateurs", OffresController.indicators);
app.get("/offresParVille", OffresController.findOffersByCity);
app.get("/companiesWithMostOffers", OffresController.findCompaniesWithMostOffers);
app.get("/favoriteOffers", OffresController.findFavoriteOffersById);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});




